package com.arkea.satd.sonar.csharp;

import org.sonar.api.batch.fs.FilePredicate;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.rule.CheckFactory;
import org.sonar.api.batch.rule.Checks;
import org.sonar.api.batch.sensor.Sensor;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;
import org.sonarsource.analyzer.commons.ProgressReport;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import com.arkea.satd.sonar.csharp.checks.FactoryCheck;

public class CsharpSensor implements Sensor {
    static final String LANGUAGE_KEY = "cs";
    private final Checks<Object> checks;
    private final FileSystem fileSystem;
    private final FilePredicate mainFilesPredicate;

    private static SensorContext staticContext;

    public static void setContext(SensorContext ctx) {
        staticContext = ctx;
    }

    public CsharpSensor(FileSystem fileSystem, CheckFactory checkFactory) {
        this.checks = checkFactory.create(CheckRepository.REPOSITORY_KEY).addAnnotatedChecks((Iterable<?>) CheckRepository.getCheckClasses());
        this.fileSystem = fileSystem;

        this.mainFilesPredicate = fileSystem.predicates().and(
                fileSystem.predicates().hasType(InputFile.Type.MAIN),
                fileSystem.predicates().hasLanguage(LANGUAGE_KEY));
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor
                .onlyOnLanguage(LANGUAGE_KEY)
                .name("BOX Csharp Sensor");
    }

    @Override
    public void execute(SensorContext context) {

        // Catch the context
        CsharpSensor.setContext(context);

        List<InputFile> inputFiles = new ArrayList<>();
        fileSystem.inputFiles(mainFilesPredicate).forEach(inputFiles::add);

        if (inputFiles.isEmpty()) {
            return;
        }

        ProgressReport progressReport = new ProgressReport("Report about progress of BOX Csharp analyzer", TimeUnit.SECONDS.toMillis(10));
        progressReport.start(inputFiles.stream().map(InputFile::toString).collect(Collectors.toList()));

        boolean cancelled = false;
        try {
            for (Object check : checks.all()) {
                FactoryCheck factoryCheck = (FactoryCheck) check;
                factoryCheck.rule(context, inputFiles);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (!cancelled) {
                progressReport.stop();
            } else {
                progressReport.cancel();
            }
        }
    }

    public static SensorContext getContext() {
        return staticContext;
    }
}
