package com.arkea.satd.sonar.csharp;

import org.sonar.api.server.profile.BuiltInQualityProfilesDefinition;
import org.sonarsource.analyzer.commons.BuiltInQualityProfileJsonLoader;

public final class CsharpSonarWayProfile implements BuiltInQualityProfilesDefinition {

    public static final String PROFILE_NAME = "Sonar way Box";

    private static final String SONAR_WAY_PATH = "org/sonar/l10n/csharp/rules/csharp/__Box_profile.json";

    @Override
    public void define(Context context) {
        NewBuiltInQualityProfile sonarWay = context.createBuiltInQualityProfile(CsharpSonarWayProfile.PROFILE_NAME, CsharpSensor.LANGUAGE_KEY);
        BuiltInQualityProfileJsonLoader.load(sonarWay, CheckRepository.REPOSITORY_KEY, CsharpSonarWayProfile.SONAR_WAY_PATH);
        sonarWay.done();
    }

}
