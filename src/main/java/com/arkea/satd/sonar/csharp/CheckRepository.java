package com.arkea.satd.sonar.csharp;

import com.arkea.satd.sonar.csharp.checks.FactoryCheck;

import java.util.Arrays;
import java.util.List;


public class CheckRepository {

    public static final String REPOSITORY_KEY = "box-csharp";
    public static final String REPOSITORY_NAME = "BOX Csharp";

    private CheckRepository() {
    }

    public static List<Class<?>> getCheckClasses() {
        return Arrays.asList(
            FactoryCheck.class
        );
    }

}
