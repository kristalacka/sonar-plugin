package com.arkea.satd.sonar.csharp;

import org.sonar.api.server.rule.RulesDefinition;
import org.sonarsource.analyzer.commons.RuleMetadataLoader;

public final class CsharpRulesDefinition implements RulesDefinition {
    public final String RESOURCE_PATH = "org/sonar/l10n/csharp/rules/csharp";
    public final String BOX_PATH = "org/sonar/l10n/csharp/rules/csharp/__Box_profile.json";

    @Override
    public void define(Context context) {
        NewRepository repository = context.createRepository(CheckRepository.REPOSITORY_KEY, CsharpSensor.LANGUAGE_KEY).setName(CheckRepository.REPOSITORY_NAME);

        RuleMetadataLoader ruleMetadataLoader = new RuleMetadataLoader(RESOURCE_PATH, BOX_PATH);

        // add the new checks
        ruleMetadataLoader.addRulesByAnnotatedClass(repository, CheckRepository.getCheckClasses());

        repository.done();
    }

}
