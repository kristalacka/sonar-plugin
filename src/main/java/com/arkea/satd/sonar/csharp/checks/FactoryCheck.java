/*
 * Copyright 2017 Credit Mutuel Arkea
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.arkea.satd.sonar.csharp.checks;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.arkea.satd.sonar.csharp.CheckRepository;

import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.sensor.issue.NewIssue;
import org.sonar.api.batch.sensor.issue.NewIssueLocation;
import org.sonar.api.rule.RuleKey;
import org.sonar.check.Rule;
import org.sonar.api.utils.log.*;


@Rule(key = "FactoryCheck")
public class FactoryCheck {
    public static RuleKey key = RuleKey.of(CheckRepository.REPOSITORY_KEY, "FactoryCheck");
    private static final Logger LOGGER = Loggers.get(FactoryCheck.class);

    public FactoryCheck() {
        super();
    }

    public void rule(SensorContext context, List<InputFile> inputFiles) throws IOException
    {
        List<String> objectWithFactories = getObjectsWithFactories(inputFiles);

        for (InputFile csharpFile : inputFiles) {
            try {
                String fileContents = csharpFile.contents();
                String fileName = csharpFile.filename();
                Pattern objectCreationPattern = Pattern.compile("= new [a-zA-Z0-9]*\\(");
                Matcher matcher = objectCreationPattern.matcher(fileContents);
                while (matcher.find()) {
                    String objectName = matcher.group();
                    objectName = objectName.substring(6, objectName.length() - 1);
                    if (objectWithFactories.contains(objectName) && !fileName.toLowerCase().contains("factory")) {
                        NewIssue newIssue = context.newIssue().forRule(key);

                        int lineNumber = getLine(fileContents, matcher.start());

                        NewIssueLocation location = newIssue.newLocation().on(csharpFile)
                                .at(csharpFile.selectLine(lineNumber)).message("Factory class exists for this object");

                        newIssue.at(location);
                        newIssue.save();
                    } else if (objectWithFactories.contains(objectName)) {
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static int getLine(String data, int start) {
        int line = 1;
        Pattern pattern = Pattern.compile("\n");
        Matcher matcher = pattern.matcher(data);
        matcher.region(0, start);
        while (matcher.find()) {
            line++;
        }
        return (line);
    }

    private static List<String> getObjectsWithFactories(Iterable<InputFile> csharpFiles) {
        List<String> objectWithFactories = new ArrayList<>();

        for (InputFile csharpFile : csharpFiles) {
            try {
                String fileContents = csharpFile.contents();
                Pattern factoryClassPattern = Pattern.compile("class [a-zA-Z0-9]*Factory");
                Matcher matcher = factoryClassPattern.matcher(fileContents);
                while (matcher.find()) {
                    String objectName = matcher.group().replace("Factory", "").replace("class ", "");
                    System.out.println("OBJECT FACTORY:" + objectName);
                    objectWithFactories.add(objectName);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return objectWithFactories;
    }

}
